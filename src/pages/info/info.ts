import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, Events } from 'ionic-angular';
import { DataProvider } from '../../providers/data';
import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer} from '@angular/platform-browser';

@Pipe({ name: 'safe' })
export class SafePipe implements PipeTransform {
  constructor(private sanitizer: DomSanitizer) {}
  transform(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
} 

@Component({
  selector: 'page-info',
  templateUrl: 'info.html'
})
export class InfoPage {
  gaViewName = 'InfoPage';
  header_data: any;
  video;
  public isIframeLoaded = false;
  
  @ViewChild("video")
  diagnosticsComponent: ElementRef;
  @ViewChild("container")
  container: ElementRef;

  constructor(public navCtrl: NavController, private data: DataProvider, public events: Events) {
    this.header_data = { title: "Proč koupit?", backButton: false };
  }

  ionViewDidEnter() {
    this.events.publish('tabs-page:badge-update', 'info', 0);
    this.diagnosticsComponent.nativeElement.src = this.data.getArticles()['link-video'];
    var clientWidth = this.container.nativeElement.clientWidth;
    this.container.nativeElement.style.height = clientWidth * 9 / 16 + "px";
    this.isIframeLoaded = false;
  }

  // na Androidu porebuji zastavit prehravani videa, kdyz prejdu na jinou zalozku
  ionViewWillLeave() {
    this.diagnosticsComponent.nativeElement.src = "";
  }

  // manualni upraveni rozmeru videa. Iframe se sam neprizpusobi
  iframeLoaded() {
    var clientWidth = this.diagnosticsComponent.nativeElement.clientWidth;
    this.diagnosticsComponent.nativeElement.height = clientWidth * 9 / 16;
    this.isIframeLoaded = true;
  }
  
}
