import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, Platform, Events } from 'ionic-angular';
import { DataProvider } from '../../providers/data';
import { Storage } from '@ionic/storage';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';

@Component({
  selector: 'page-contest',
  templateUrl: 'contest.html'
})
export class ContestPage {
  gaViewName = 'ContestPage';
  header_data: any;

  @ViewChild("video")
  diagnosticsComponent: ElementRef;
  loggedIn = false;
  userData: any;

  constructor(public navCtrl: NavController, private data: DataProvider, private platform: Platform, public events: Events, private storage: Storage, private fb: Facebook) {
    this.header_data = { title: "Soutěže", backButton: false };

    this.storage.get('facebookUserData').then((data) => {
      if (data) {
        console.log('storage facebookUserData', data);
        this.userData = data;
        this.loggedIn = true;
        this.showContestIframe();
      }
    });
  }

  ionViewDidEnter() {
      this.events.publish('tabs-page:badge-update', 'contest', 0);
  }

  loginFacebook() {
    this.fb.login(['public_profile'])
      .then((res: FacebookLoginResponse) => {
        console.log('Logged into Facebook!', res);
        this.loggedIn = true;
        this.fb.api('me?fields=id,email,first_name,last_name,birthday,gender,hometown,location', []).then(profile => {
console.log('Facebook profile', profile);
          this.userData = { email: profile['email'], first_name: profile['first_name'], last_name: profile['last_name'], birthday: profile['user_birthday'] };
          console.log('Facebook userdata', this.userData);
          this.storage.set('facebookUserData', this.userData);
          this.showContestIframe();
        });        
      })
      .catch(e => {
        console.log('Error logging into Facebook', e);
      });
  }  

  async showContestIframe() {
    var clientWidth = this.platform.width(); // this.diagnosticsComponent.nativeElement.clientWidth;
    var clientHeight = this.diagnosticsComponent.nativeElement.clientHeight;
    console.log("clientWidth:" + clientWidth);
    console.log("clientHeight:" + clientHeight);
    clientWidth = Math.min(clientWidth, 500);
    console.log("clientWidth:" + clientWidth);
    if (!this.diagnosticsComponent.nativeElement.src) {

        let srcArray = await this.storage.get("contests");
        if (srcArray && srcArray.length > 0) {
          let url = srcArray[0].contestUrl + "?email=" + this.userData.email + "&first_name=" + this.userData.first_name + "&last_name=" + this.userData.last_name + "&birthday=" + this.userData.birthday;
          console.log("iframe src", url);
          this.diagnosticsComponent.nativeElement.src = url; // mela by byt vzdy jedna soutez
        }

        this.diagnosticsComponent.nativeElement.width = clientWidth;
        // this.diagnosticsComponent.nativeElement.src = "https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Ftarespa%2F&tabs=timeline&width=" + clientWidth + "&height=" + clientHeight + "&small_header=true&adapt_container_width=true&hide_cover=true&show_facepile=false&appId=207808999413453";
    }
  }



}
