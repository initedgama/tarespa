import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { DetailPage } from '../detail/detail';
import { DataProvider } from '../../providers/data';
import { Events } from 'ionic-angular';
import { NotificationsProvider } from '../../providers/notifications';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  gaViewName = 'HomePage';
  searchQuery: string = '';
  models;
  firstLoad: boolean = true;

  constructor(public navCtrl: NavController,
              private data: DataProvider,
              public events: Events,
              private toastCtrl: ToastController,
              private pushProv: NotificationsProvider) {
    events.subscribe('models:loaded', (time) => {
      console.log('models:loaded', time);
      this.models = this.data.getModels();
    });    
  }
  
  ionViewDidEnter() {
    this.firstLoad ? this.firstLoad = false : this.events.publish('tabs-page:badge-update', 'models', 0);
  }

  getItems(ev: any) {
    // prihlaseni na topic test (notifikace)
    if (ev.target.value && ev.target.value.toLowerCase() == 'test') {
      this.pushProv.updateTopic("test")
          .then((data) => {
              const toast = this.toastCtrl.create({
                  message: "Byl jste přihlášen do kanálu test",
                  duration: 3000
              });
              toast.present();
          })
          .catch(error => {console.log(error);})
    }
    this.models = this.data.search(ev.target.value);
  }

  goToDetailPage(model) {
    this.navCtrl.push(DetailPage, { id: model });
  }

}
