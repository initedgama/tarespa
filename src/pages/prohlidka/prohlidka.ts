import { Component, NgZone} from '@angular/core';
import { AlertController, NavController, NavParams } from 'ionic-angular';
import { Zip } from '@ionic-native/zip';
import { FileTransferObject, FileTransfer } from '@ionic-native/file-transfer';
import { DirectoryEntry, DirectoryReader, Entry, File, FileEntry } from '@ionic-native/file';
import { DataProvider } from '../../providers/data';

@Component({
  selector: 'page-prohlidka',
  templateUrl: 'prohlidka.html'
})
export class ProhlidkaPage {
  header_data: any;
  imagesDirectory: string;
  showSpinner: boolean = false;
  modelId: string;
  cacheDir: string;
  isDruhyPokusProUnzip = false; // na iOS se vetsinou zip nerozbali na prvni pokus, proto pomoci flagu ho zkusim rozzipovat 2x

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private zip: Zip,
              private transfer: FileTransfer,
              private _ngZone: NgZone,
              private alertCtrl: AlertController,
              private dataProv: DataProvider,
              private file: File) {
    this.header_data = { title: "3D model", backButton: true };
    this.init(navParams.get("model"));
  }

  async init(model) {
      this.cacheDir = this.file.cacheDirectory;
      this.showSpinner = true;
      if (model && model.model) {
          this.modelId = model.id + "";
          let oldModel = this.dataProv.getOldModel(model.id);

          // pokud novy model ma url, nebo pokud se timestamp noveho a stareho modelu lisi (doslo ke zmene),
          // tak nactu 360 view z noveho model
          if (!oldModel && model.model.url) {
              this.download(model);
          } else if (model.model.url && oldModel.model && oldModel.model.url != model.model.url) {
              this.download(model);
          } else {
              // jinak zkusim nacist data z cache
              console.log("beru data z cache");
              this.getFromCache(model);
          }
      } else {
          this.showError("nemam model a info o nem");
      }
  }

    getFromCache(model) {
        this.file.resolveDirectoryUrl(this.cacheDir + "/" + this.modelId)
            .then((dest: DirectoryEntry) => {
                // zkontroluju jestli ve slozce jsou nejake obrazky
                let dirReader: DirectoryReader = dest.createReader();
                dirReader.readEntries((data: Entry[]) => {
                    if (data && data.length > 0) {
                        console.log("ve slozce by mely byt obrazky");
                        // zone > aby angular poznal zmenu promenne
                        this._ngZone.run(() => {
                            this.imagesDirectory = dest.toURL();
                            this.showSpinner = false;
                        });
                    } else {
                        console.log("zip byl prazdny");
                        this.showError("zip byl prazdny", "Info", "Tento model nemá momentálně 360° pohled");
                    }
                });
                this._ngZone.run(() => {
                    this.imagesDirectory = dest.toURL();
                    this.showSpinner = false;
                });
            })
            .catch((error: any) => {
                console.log("data nejsou v cache, tak je stahnu", error);
                this.download(model);
            });
    }

  download(model) {
      if (model.model.downloaded == true) {
          console.log("uz mam stazeno");
          this.getFromCache(model);
          return;
      }
      console.log("download model 360" + model.model.url);
      let fileTransferObject: FileTransferObject = this.transfer.create();
      fileTransferObject.download(model.model.url, this.cacheDir + "/model_detail.zip").then((entry: FileEntry) => {
          // download ok
          if (entry) {
              this.file.resolveDirectoryUrl(this.cacheDir)
                  .then(async (dest: DirectoryEntry) => {
                      dest.getDirectory(this.modelId, {create: false},
                          // smaz stary obsah
                          (oldDir: DirectoryEntry) => {
                              oldDir.removeRecursively(() => {
                                  this.createDirAndUnzip(dest, entry, model);
                              }, (error) => {this.showError("chyba behem mazani stareho obsahu " + error)})
                          },
                          // pokud driv nebyly zadna data, pokracuju
                          (error) => {
                              console.log("byla snaha smazat stary obsah zipu, asi stara slozka nebyla na disku. ", error);
                              this.createDirAndUnzip(dest, entry, model);
                          });
                  })
                  .catch((error) => {this.showError(error)});
          }
      }).catch((error) => {this.showError(error)});
  }

    createDirAndUnzip(cacheDir: DirectoryEntry, zip: FileEntry, model: any) {
        cacheDir.getDirectory(this.modelId, {create: true},
            (destDir: DirectoryEntry) => {
                this.zip.unzip(zip.toURL(), destDir.toURL()).then((res: any) => {
                    console.log("stazeno:" + res);
                    console.log("druhy pokus " + this.isDruhyPokusProUnzip);
                    if (res == -1) {
                        if (this.isDruhyPokusProUnzip) {
                            this.isDruhyPokusProUnzip = true;
                            this.tryAgain(model);
                            //this.showError("Chyba behem rozzipovani souboru");
                        } else {
                            this.isDruhyPokusProUnzip = true;
                            this.init(model);
                        }
                    } else {
                        console.log("rozzipovani OK");
                        let dirReader: DirectoryReader = destDir.createReader();
                        dirReader.readEntries((data: Entry[]) => {
                            if (data && data.length > 0) {
                                console.log("soubory rozzipovany a jsou ok");
                                // zone > aby angular poznal zmenu promenne
                                this._ngZone.run(() => {
                                    model.model.downloaded = true;
                                    this.imagesDirectory = destDir.toURL();
                                    this.showSpinner = false;
                                });
                            } else {
                                console.log("zip byl prazdny");
                                this.showError("zip byl prazdny", "Info", "Tento model nemá momentálně 360° pohled");
                            }
                        });
                    }
                }).catch((reason: any) => {
                    this.showError("nepovedlo se rozzipovat soubor: " + reason);
                });
                zip.remove(() => {
                }, (error) => {
                    console.log("nepovedlo se odstranit stary zip: " + error);
                })
            }, (error) => {this.showError(error)});
    }

    showError(reason: any, title = "Chyba", msg = "3D model se nepodařilo načíst") {
        console.log(reason);
        this.showSpinner = false;
        this.alertCtrl.create({
            title: title,
            message: msg,
            buttons: [
                {
                    text: "OK",
                    role: "cancel"
                }
            ]
        }).present();
    }

    tryAgain(model: any) {
        this.showSpinner = false;
        this.alertCtrl.create({
            title: "Chyba během stahování 3D modelu",
            message: "Pokusit se znovu stáhnout 3d model?",
            buttons: [
                {
                    text: "Stáhnout",
                    handler: () => {
                        this.isDruhyPokusProUnzip = false;
                        this.init(model);
                    }
                }, 
                {
                    text: "Zrušit",
                    role: "cancel"
                }
            ]
        }).present();
    }
}
