import { Component } from '@angular/core';
import { NavController, Events } from 'ionic-angular';
import { DataProvider } from '../../providers/data';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {
  gaViewName = 'ContactPage';
  header_data: any;
  text;

  constructor(public navCtrl: NavController, private data: DataProvider, private inAppBrowser: InAppBrowser, public events: Events) {
    this.header_data = { title: "O nás - kontakty", backButton: false };
     
    this.text = this.data.getArticles()['o-nas'];
  }

  ionViewDidEnter() {
    this.events.publish('tabs-page:badge-update', 'contact', 0);
  }

  openBrowser(link) {
    this.inAppBrowser.create(this.data.getArticles()[link], "_system");
  }

  textOnclick(ev) {
    if (ev.target && ev.target.href) {
      console.log("Click on <a> in text, href:" + ev.target.href);
      this.inAppBrowser.create(ev.target.href, "_system");
      ev.preventDefault();
    }
  }

}
