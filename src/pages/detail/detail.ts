import { Component, ViewChild } from '@angular/core';
import {AlertController, NavController, NavParams, Slides} from 'ionic-angular';
import { ProhlidkaPage } from '../prohlidka/prohlidka';
import { DataProvider } from '../../providers/data';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'page-detail',
  templateUrl: 'detail.html'
})
export class DetailPage {
  gaViewName = 'DetailPage';
  @ViewChild(Slides) slides: Slides;
  model;  // model, ktery zobrazuju v sablone (bude se prepinat mezi standard a silver)
  variant;
  photos;
  hasCrystal = false;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private data: DataProvider,
              private alertCtrl: AlertController,
              private inAppBrowser: InAppBrowser) {
    this.model = this.data.getModel(navParams.get("id"));
    this.photos = this.model.photos;
    this.variant = this.model.category;
    this.hasCrystal = this.data.hasModelCrystal(this.model);
    this.gaViewName = this.model.title;
  }

  goToProhlidkaPage() {
    if (this.model) {
        this.navCtrl.push(ProhlidkaPage, {model: this.model});
    }
  }

  variantChanged() {
    this.photos = this.data.getModelPhotos(this.model, this.variant);
    this.model = this.data.getModelCrystal(this.model, this.variant);
    this.slides.slideTo(0, 0);
  }

  buy() {
    this.inAppBrowser.create(this.model.buyUrl, "_system");
  }

  view() {
    this.inAppBrowser.create(this.model.viewUrl, "_system");
  }

}
