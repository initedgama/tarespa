import { Component, ViewChild } from '@angular/core';
import { NavParams, Tabs, Events } from 'ionic-angular';

import { HomePage } from '../home/home';
import { ContestPage } from '../contest/contest';
import { InfoPage } from '../info/info';
import { ContactPage } from '../contact/contact';

import { SwipeTabDirective } from '../../directives/swipe-tab.directive';
import { DataProvider } from '../../providers/data';

@Component({
    templateUrl: 'tabs.html'
})
export class TabsPage {
    @ViewChild(SwipeTabDirective) swipeTabDirective: SwipeTabDirective;
    @ViewChild('myTabs') tabRef: Tabs;

    tab1Root = HomePage;
    tab2Root = ContestPage;
    tab3Root = InfoPage;
    tab4Root = ContactPage;

    mySelectedIndex;

    tabBadgeCount = {'models': 0, 'contest': 0, 'info': 0, 'contact': 0};

    constructor(public navParams: NavParams, public events: Events, private data: DataProvider) {
        this.mySelectedIndex = navParams.data.tabIndex || 0;
        this.subscribeToBadgeCountChange();
    }

    subscribeToBadgeCountChange() {
        this.events.subscribe('tabs-page:badge-update', (tab, count) => {
            console.log('refreshBadgeCount', tab, count);
            this.tabBadgeCount[tab] = count;
        });
    }

    // tohle dělá jen přepnutí
    transition($event) {
        console.log('transition ' + $event.index);
        this.swipeTabDirective.onTabInitialized($event.index);
        this.swipeTabDirective.setCurrentTab($event.index);
    }

    // tohle dělá přepnutí i swipe
    onTabChange(index: number) {
        console.log('onTabChange ' + index);
        this.tabRef.select(index);
    }
}
