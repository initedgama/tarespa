import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';
import { NotificationsProvider } from '../providers/notifications';
import { CacheProvider } from '../providers/cache';
import { DataProvider } from '../providers/data';
import { AnalyticsProvider } from "../providers/analytics";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage:any = TabsPage;

  constructor(private platform: Platform,
              private statusBar: StatusBar,
              private splashScreen: SplashScreen,
              private cacheProv: CacheProvider,
              private dataProv: DataProvider,
              private notificationsProvider: NotificationsProvider,
              private analytics: AnalyticsProvider) {
    this.platform.ready().then(() => {
        console.log("--- Device Is Ready ---");
        // Okay, so the platform is ready and our plugins are available.
        // Here you can do any higher level native things you might need.
        this.statusBar.styleDefault();
//      setTimeout( () => { splashScreen.hide(); }, 1000);
        this.cacheProv.init();
        this.analytics.init();
        this.notificationsProvider.notificationSetup(this.nav);
    });
  }
}
