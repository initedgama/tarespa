import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp, } from './app.component';

import { CustomHeaderComponent } from '../components/custom-header/custom-header';
import { DataProvider } from '../providers/data';
import { NotificationsProvider } from '../providers/notifications';
import { AnalyticsProvider } from "../providers/analytics";

import { HomePage } from '../pages/home/home';
import { DetailPage } from '../pages/detail/detail';
import { ProhlidkaPage } from '../pages/prohlidka/prohlidka';
import { ContestPage } from '../pages/contest/contest';
import { InfoPage } from '../pages/info/info';
import { SafePipe } from '../pages/info/info';
import { ContactPage } from '../pages/contact/contact';
import { TabsPage } from '../pages/tabs/tabs';
import { SwipeTabDirective } from '../directives/swipe-tab.directive';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { FCM } from '@ionic-native/fcm';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';
import { RotatorComponent } from '../components/rotator/rotator';
import { FileTransfer } from "@ionic-native/file-transfer";
import { File } from "@ionic-native/file";
import { Zip } from "@ionic-native/zip";
import { CommonModule } from "@angular/common";
import { CacheProvider } from '../providers/cache';
import { Facebook } from '@ionic-native/facebook';

export const baseUrl: string = 'https://appadmin.tarespa.com';
export const baseUrlApi: string = baseUrl + '/api';
export const analyticsKey: string = "UA-135702264-1";

@NgModule({
  declarations: [
    MyApp,
    CustomHeaderComponent,
    RotatorComponent,
    HomePage, ContestPage, InfoPage, ContactPage, DetailPage, ProhlidkaPage, 
    TabsPage,
    SwipeTabDirective,
    SafePipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    CommonModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage, ContestPage, InfoPage, ContactPage, DetailPage, ProhlidkaPage, 
    TabsPage
  ],
  providers: [
    File,
    FileTransfer,
    Zip,
    StatusBar,
    SplashScreen,
    InAppBrowser,
    FCM,
    DataProvider,
    NotificationsProvider,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    CacheProvider,
    AnalyticsProvider,
    GoogleAnalytics,    
    Facebook
  ]
})
export class AppModule {}
