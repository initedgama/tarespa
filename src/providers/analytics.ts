import { GoogleAnalytics } from "@ionic-native/google-analytics";
import { Injectable } from "@angular/core";
import { Platform } from "ionic-angular";
import { App, ViewController } from "ionic-angular";
import { analyticsKey } from "../app/app.module";

@Injectable()
export class AnalyticsProvider {

  constructor(
      private ga: GoogleAnalytics,
      private platform: Platform,
      private app: App) {
  }

  public init() {
    console.log("Hello AnalyticsProvider Provider");
    if (!this.platform.is("cordova")) {
      console.log("Neni cordova");
      this.app.viewDidEnter.subscribe(
              (view: ViewController) => {
                  console.log("New view:" + view.id);
              }
            );
      return;
    }

    this.ga.startTrackerWithId(analyticsKey)
      .then(() => {
        console.log("Google analytics is ready now");
        // the component is ready and you can call any method here
        // this.ga.debugMode();
        this.ga.setAllowIDFACollection(false);
        this.ga.trackView("IntroPage"); // Informace o prvni strance se ztrati, tak ji doplnim
        this.app.viewDidEnter.subscribe(
          (view: ViewController) => {
              if (view.instance.gaViewName) {
                console.log("New view:" + view.instance.gaViewName);
                this.ga.trackView(view.instance.gaViewName);
              }
          }
        );
      })
      .catch(e => console.log("Error starting GoogleAnalytics", e));
  }
}
