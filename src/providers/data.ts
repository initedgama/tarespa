import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { Events } from 'ionic-angular';
import { baseUrlApi } from '../app/app.module';

@Injectable()
export class DataProvider {
    models = Array();
    oldModels = Array();
    articles = {};
    contests = [];

    constructor(private http: HttpClient, private storage: Storage, public events: Events) {
    }


    getArticlesFromStorage() {
        return this.storage.get("articles").then((data) => {
            if (data != null) {
                this.articles = data;
                console.log('articles loaded from storage');
                return data;
            }
        });
    }

    getContestsFromStorage() {
        return this.storage.get("contests").then((data) => {
            if (data != null) {
                this.contests = data;
                console.log('contests loaded from storage');
                return data;
            }
        });
    }

    getModelsFromStorage() {
        return this.storage.get("models").then((data) => {
            if (data != null) {
                this.models = data;
                console.log('models loaded from storage');
                return this.models;
            }
        });
    }

    async getArticlesFromApi() {
        try {
            return await this.http.get(baseUrlApi + '/articles')
                .map(async (data: any) => {
                    this.articles = {};
                    data.forEach(element => {
                        this.articles[element.key] = element.text;
                    });
                    console.log('articles loaded from API');
                    this.storage.set("articles", this.articles);
                    return this.articles;
                }).toPromise();
        } catch (error) {
            console.log("maybe connection error");
            console.log("Oooops! " + JSON.stringify(error));
            return undefined;
        }
    }

    async getContestFromApi() {
        try {
            return await this.http.get(baseUrlApi + '/contests')
                .map(async (data: any) => {
                    this.contests = data;

                    console.log('contests loaded from API:' + this.contests);
                    this.storage.set("contests", this.contests);
                    return this.contests;
                }).toPromise();
        } catch (error) {
            console.log("maybe connection error");
            console.log("Oooops! " + JSON.stringify(error));
            return undefined;
        }
    }

    async getModelsFromApi() {
        try {
            return await this.http.get(baseUrlApi + '/models').map(
                async (data: any) => {
                    let models = [];
                    data.forEach(element => {
                        if (element.active) {
                            let photos = Array();
                            element.gallery.forEach(photo => {
                                photos.push({url: photo.url, timestamp: photo.timestamp});
                            });
                            models.push({
                                id: element.id,
                                updated: element.updated,
                                category: element.category,
                                photos: photos,
                                picture: element.picture,
                                title: element.title,
                                description: element.description,
                                buyUrl: element.buyUrl,
                                viewUrl: element.viewUrl,
                                model: element.model,
                                timestamp: element.model.timestamp
                            });
                        }
                    });
                    this.models = models;
                    this.storage.set("models", models);
                    console.log('models loaded from API');
                    this.events.publish('models:loaded', Date.now());
                    return this.models;
                }).toPromise();
        } catch (error) {
            console.log("maybe connection error");
            console.log("Oooops! " + JSON.stringify(error));
            return undefined;
        }
    }

    getModel(id) {
        if (!this.models) {
            return null;
        }
        let model;
        this.models.forEach(element => {
            if (element.id == id) {
                model = element;
            }
        });
        return model;
    }

    getOldModel(id) {
        if (!this.oldModels) {
            return null;
        }
        let model;
        this.oldModels.forEach(element => {
            if (element.id == id) {
                model = element;
            }
        });
        return model;
    }


    getModelFromModels(id, models) {
        if (!models) {
            return null;
        }
        let model;
        models.forEach(element => {
            if (element.id == id) {
                model = element;
            }
        });
        return model;
    }

    // vraci fotky modelu dle zvolene varianty
    getModelPhotos(model, variant) {
        let photos;
        this.models.forEach(element => {
            if (element.title == model.title && element.category == variant) {
                photos = element.photos;
            }
        });
        return photos;
    }

    // zjisti, zda existuje kristalova varianta stejneho modelu dle jeho nazvu
    hasModelCrystal(model) {
        let hasCrystal = false;
        this.models.forEach(element => {
            if (element.title == model.title && element.category == "silver") {
                hasCrystal = true;
            }
        });
        return hasCrystal;
    }
    // vrati kristalovou variantu modelu
    getModelCrystal(model, variant = "standard") {
        if (!model) {
            return undefined;
        }
        let kristal = undefined;
        this.models.forEach(element => {
            if (element.title == model.title && element.category == variant) {
                kristal = element;
            }
        });
        return kristal;
    }

    getArticles() {
        return this.articles;
    }
    getModels() {
        return this.models;
    }
    setModels(models) {
        this.models = models;
    }

    getOldModels() {
        return this.oldModels;
    }

    search(queryText = '') {
        let result = [];
        // malá písmena a odstranění diakritiky
        queryText = queryText.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, "");
        this.getModels().forEach(element => {
            // hledat v názvu týmu i popisu
            let text = element.title.toLowerCase() + element.description.toLowerCase();
            // odstranění diakritiky
            text = text.normalize('NFD').replace(/[\u0300-\u036f]/g, "");
            if (text.indexOf(queryText) != -1) {
                result.push(element);
            }
        });
        return result;
    }
}