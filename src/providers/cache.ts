import { Injectable } from '@angular/core';
import { Events, ToastController } from 'ionic-angular';
import { DirectoryEntry, File, FileEntry } from '@ionic-native/file';
import { FileTransfer, FileTransferObject} from '@ionic-native/file-transfer';
import { DataProvider } from './data';
import { SplashScreen } from '@ionic-native/splash-screen';

declare var Ionic;

/*
  Generated class for the CacheProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CacheProvider {

    models = Array();
    articles = {};
    oldModels = Array();
    oldArticles = {};
    cacheDir: string;
    shouldWorkOffline = true;

    constructor(private file: File,
                private dataProv: DataProvider,
                private events: Events,
                private splash: SplashScreen,
                private toastCtrl: ToastController,
                private transfer: FileTransfer) {
    }

    /**
     * Vezmu ze storage stare modely, z api si vezmu nove, pak je oba porovnavam, a pokud mam stejne obrazky,
     * tak je vezmu s cache (pokud tam nejsou, stahnu je a ulozim). Pokud mam ruzne obrazky, vezmu obrazek
     * z noveho modelu a stahnu ho a ulozim.
     *
     * Nakonec znova porovnam oba modely a obrazky co se lisi a jsou v cache, tak je z ni smazu
     */
    public async init() {
        console.log("cache provider init");
        this.cacheDir = this.file.cacheDirectory;
        this.oldArticles = await this.dataProv.getArticlesFromStorage();
        this.articles = await this.dataProv.getArticlesFromApi();
        this.articles = this.articles ? this.articles : this.oldArticles;
        this.articles = this.articles ? this.articles : [];

        this.oldModels = await this.dataProv.getModelsFromStorage();
        this.dataProv.oldModels = this.oldModels;

        this.models = await this.dataProv.getModelsFromApi();
        this.models = this.models ? this.models : this.oldModels;
        this.models = this.models ? this.models : [];

        this.countNews();

        if (this.shouldWorkOffline) {
            await this.initCacheAndImages();
        }

        console.log("konec");
        this.dataProv.setModels(this.models);
        this.events.publish('models:loaded', Date.now());
        this.splash.hide();
        const toast = this.toastCtrl.create({
            message: "Data aktualizována",
            duration: 3000
        });
        toast.present();

    }

    private async initCacheAndImages() {
        console.log("start download to cache");
        for (let model of this.models) {
            let oldModel = this.dataProv.getModelFromModels(model.id, this.oldModels);
            console.log(model);
            // mam stary model
            if (oldModel) {
                // get picture
                // updated model
                if (model.picture && oldModel.picture) {
                    let newUrl = "";
                    if (model.picture.url !== oldModel.picture.url) {
                        console.log("BUDU STAHOVAT");
                        newUrl = await this.downloadAndSaveToCache(model.picture.url, oldModel.picture.url);
                    } else {
                        console.log("BERU Z CACHE");
                        newUrl = await this.getFromCache(model.picture.url);
                    }
                    model.picture.url = newUrl ? newUrl : model.picture.url;
                    console.log("ulozim:" + model.picture.url);
                }

                console.log("jdu na galerii");
                // get gallery
                if (model.photos) { // v novym modelu mam galerii
                    if (oldModel.photos) {  // ve starym mam galerii
                        for (let newPhoto of model.photos) {

                            let isInCache = false;
                            let newPhotoUrl = "";

                            for (let oldPhoto of oldModel.photos) {
                                // TODO smazat az pujde server
                                if (newPhoto.url == oldPhoto.url) {
                                    // beru z cache
                                    newPhotoUrl = await this.getFromCache(newPhoto.url);

                                    isInCache = true;
                                    break;
                                }
                            }
                            if (!isInCache) {
                                // download and save
                                newPhotoUrl = await this.downloadAndSaveToCache(newPhoto.url);
                            }
                            newPhoto.url = newPhotoUrl ? newPhotoUrl : newPhoto.url;
                            console.log("ulozim:" + newPhoto.url);
                        }
                    } else {
                        // ve starym modelu nemam galerii, obrazky jsou nove
                        for (let newPhoto of model.photos) {
                            let newDownloadedUrl = await this.downloadAndSaveToCache(newPhoto.url);
                            newPhoto.url = newDownloadedUrl ? newDownloadedUrl : newPhoto.url;
                            console.log("ulozim:" + newPhoto.url);
                        }
                    }
                }
            } else {
                console.log("nemam stary model, asi prvni spusteni");
                // tento model je novy, jeste ho nemam ve storage
                let newUrl = await this.downloadAndSaveToCache(model.picture.url);
                console.log("novy model: " + newUrl);
                model.picture.url = newUrl ? newUrl : model.picture.url;
                // galerie
                if (model.photos) {
                    for (let newPhoto of model.photos) {
                        console.log("chci: " + newPhoto.url);
                        let newGalleryUrl = await this.downloadAndSaveToCache(newPhoto.url);
                        newPhoto.url = newGalleryUrl ? newGalleryUrl : newPhoto.url;
                        console.log("ulozim:" + newPhoto.url);
                    }
                }

            }
        }
        this.cleanCache();
    }

    private async countNews() {
        console.log("start count news");
        let newModelsCount: number = 0;
        let contestsCount: number = 0;
        let infoCount: number = 0;
        let aboutCount: number = 0;

        console.log(this.models);
        console.log(this.oldModels);

        // ### models
        if (this.models) {
            // rychle spocitam nove modely, pak az budu stahovat
            for (let model of this.models) {
                let oldModel = this.dataProv.getModelFromModels(model.id, this.oldModels);

                if (!oldModel) {
                    // nove modely
                    newModelsCount++;
                } else {
                    // aktualizovane modely
                    if (model.updated != oldModel.updated) {
                        newModelsCount++;
                    }
                }
            }
        }

        if (this.oldModels) {
            for (let oldModel of this.oldModels) {
                let newModel = this.dataProv.getModelFromModels(oldModel.id, this.models);
                if (!newModel) {
                    // stary model byt smazany
                    newModelsCount++;
                }
            }
        }

        // ### articles
        // proc tarespa
        if (this.oldArticles) {
            if (this.articles) {
                if (this.oldArticles['link-video'] != this.articles['link-video']) {
                    infoCount = 1;
                }
                // o nas
                if (this.oldArticles['o-nas'] != this.articles['o-nas']) {
                    aboutCount = 1;
                }
            }
        } else {
            infoCount = 1;
            aboutCount = 1;
        }

        let oldContests = await this.dataProv.getContestsFromStorage();
        let newContests = await this.dataProv.getContestFromApi();

        if (JSON.stringify(oldContests) != JSON.stringify(newContests)) {
            contestsCount = 1;
        }
        this.events.publish('tabs-page:badge-update', 'models', newModelsCount);
        this.events.publish('tabs-page:badge-update', 'contest', contestsCount);
        this.events.publish('tabs-page:badge-update', 'info', infoCount);
        this.events.publish('tabs-page:badge-update', 'contact', aboutCount);
        console.log("mam " + newModelsCount + " novych modelu");
        console.log("mam " + contestsCount + " novych soutezi");
        console.log("mam " + infoCount + " novych videi");
        console.log("mam " + aboutCount + " novych info");
    }


    private cleanCache() {
        console.log("start clean cache");
        // prvni spusteni
        if (!this.oldModels) {
            return;
        }

        for (let oldModel of this.oldModels) {
            let newModel = this.dataProv.getModelFromModels(oldModel.id, this.models);

            if (!newModel) {
                this.deleteFile(oldModel.picture.url);
                this.delete3dModel(oldModel);

                for (let oldPhoto of oldModel.photos) {
                    this.deleteFile(oldPhoto.url);
                }

            } else {

                if (oldModel.picture && newModel.picture &&
                    this.getFilename(oldModel.picture.url) != this.getFilename(newModel.picture.url)) {
                    this.deleteFile(oldModel.picture.url);
                }

                if (oldModel.model && newModel.model &&
                    this.getFilename(oldModel.model.url) != this.getFilename(newModel.model.url)) {
                    this.delete3dModel(oldModel);
                }

                // gallery
                for (let oldPhoto of oldModel.photos) {
                    let shouldDelete = true;
                    for (let newPhoto of newModel.photos) {
                        if (this.getFilename(newPhoto.url) == this.getFilename(oldPhoto.url)) {
                            shouldDelete = false;
                            break;
                        }
                    }
                    if (shouldDelete) {
                        this.deleteFile(oldPhoto.url);
                    }
                }
            }
        }
    }

    private async deleteFile(url) {
        url = this.getFilename(url);
        this.file.resolveDirectoryUrl(this.cacheDir)
            .then((cache: DirectoryEntry) => {
                cache.getFile(url, {create: false}, (file: FileEntry) => {
                    file.remove(() => {
                        console.log("entry " + file.name + " removed");
                    }, this.onError);
                })
            })
            .catch(this.onError);
    }

    private async delete3dModel(model) {
        if (!model) {
            return;
        }
        this.file.resolveDirectoryUrl(this.cacheDir + "/" + model.id)
            .then((oldDir: DirectoryEntry) => {
                oldDir.removeRecursively(() => {
                }, this.onError)
            })
            .catch(this.onError)
    }

    private onError(error) {
        console.log(error);
    }

    private async getFromCache(url: string): Promise<any> {
        return this.file.resolveDirectoryUrl(this.cacheDir)
            .then((parent: DirectoryEntry) => {
                return this.file.getFile(parent, this.getFilename(url), {create: false})
                    .then((img: FileEntry) => {
                        return Ionic.WebView.convertFileSrc(img.toURL());
                    })
                    .catch(error => {
                        console.log("ERROR get img from cache: " + url, error);
                        return this.downloadAndSaveToCache(url);
                    });
            })
            .catch(error => {
                console.log("ERROR resolve dir getFromCache", error);
                return url;
            });
    }

    private async downloadAndSaveToCache(url: string, oldPath?: string): Promise<string> {
        try {
            let cacheDirectory: DirectoryEntry = await this.file.resolveDirectoryUrl(this.cacheDir);
            this.removeOld(cacheDirectory, oldPath);
            return this.downloadImage(url)
                .then((data) => {
                    if (data) {
                        return Ionic.WebView.convertFileSrc(data)
                    } else {
                        return url;
                    }
                })
                .catch(error => {
                    console.log("NEmam vse stazeno. chyba: " + error);
                    return url;
                });

        } catch (error) {
            console.log("try-catch: " + error);
        }
        return url;
    }

    private downloadImage(url: string): Promise<string> {
        if (!url) {
            return;
        }
        let fileName = this.getFilename(url);
        let fileTransferObject: FileTransferObject = this.transfer.create();
        console.log("url to download:" + url);
        return fileTransferObject.download(url, this.cacheDir + "/" + fileName)
            .then(() => {
                return this.cacheDir + "/" + fileName;
            })
            .catch((error) => {
                console.log("ERROR: download:" + url, error);
                return url;
            });
    }

    private async removeOld(parent: DirectoryEntry, path: string) {
        if (path) {
            this.file.getFile(parent, this.getFilename(path), {create: false})
                .then((oldImg: FileEntry) => {
                    oldImg.remove(() => {
                    }, error => {
                        console.log("ERRRO during removing old file: ", error);
                    });
                })
                .catch(error => {
                    console.log("ERRRO canot get old file: ", error);
                })
        }
    }

    private getFilename(url: string): string {
        if (!url) {
            return "";
        }
        return url.substring(url.lastIndexOf('/') + 1);
    }
}
