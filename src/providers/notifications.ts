import { Injectable } from '@angular/core';
import { NavController, Platform, ToastController } from 'ionic-angular';
import { FCM } from '@ionic-native/fcm';
import { TabsPage } from '../pages/tabs/tabs';

@Injectable()
export class NotificationsProvider {
  topic = "news";
  nav: NavController;

  constructor(private platform: Platform, private fcm: FCM, public toastCtrl: ToastController) {
  }

  notificationSetup(nav: NavController) {
    if (!this.platform.is("cordova")) {
        return;
    }
    this.nav = nav;
    this.fcm.subscribeToTopic(this.topic);

    this.fcm.getToken().then(token => {
      console.log("Token:" + token);
    });

    var self = this;
    this.fcm.onNotification().subscribe(data => {
      console.log("onNotification");
      if(data.wasTapped) {
        console.log("Received in background");
        self.openSpecificPage(data.page);
      } else {
        console.log("Received in foreground");
        this.showNotification(data);
      };
    });    

    this.fcm.onTokenRefresh().subscribe(token => {
      console.log("Token:" + token);
    });

    //this.fcm.unsubscribeFromTopic(this.topic);
  }

  /**
   * Slouzi hlavne pro prihlaseni na topic "test"
   * @param topic
   */
  public updateTopic(topic: string): Promise<any> {
      console.log("add PN topic: " + topic);
      return this.fcm.subscribeToTopic(topic);
  }

  showNotification(msg) {
    console.log(msg);
    const toast = this.toastCtrl.create({
      message: msg.title + ": " +  msg.body,
      duration: 3000
    });
    toast.present();
  }

  async openSpecificPage(page: String) {
    console.log("opening page:" + page);
    switch(page) {
    case "modely":
      this.nav.push(TabsPage, {tabIndex: 0});
      break;
    case "souteze":
      this.nav.push(TabsPage, {tabIndex: 1});
      break;
    case "info":
      this.nav.push(TabsPage, {tabIndex: 2});
      break;
    case "kontakt":
      this.nav.push(TabsPage, {tabIndex: 3});
      break;
    }
  }
}
