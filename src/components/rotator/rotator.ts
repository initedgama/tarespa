import { ChangeDetectorRef, Component, Input, SimpleChange, SimpleChanges } from '@angular/core';
import { AlertController, NavController } from 'ionic-angular';
import * as $ from 'jquery'
import { DirectoryReader, Entry, File } from '@ionic-native/file';

declare var Ionic;

@Component({
    selector: 'rotator',
    templateUrl: 'rotator.html'
})
export class RotatorComponent {
    @Input("path") public path: string;
    private _images: Array<string> = [];
    private _selectedIndex = 0;
    private _size: number = 0;
    private _events: Array<any> = [];
    private _finishTimeout;
    private _defaultZoom = 1;


    constructor(public navCtrl: NavController, private file: File, private alertCtrl: AlertController, private ref: ChangeDetectorRef) {
    }

    ngOnChanges(changes: SimpleChanges) {
        const url: SimpleChange = changes.path;
        if (url && url.currentValue) {
            console.log('prev value: ', url.currentValue);
            this.file.resolveDirectoryUrl(url.currentValue).then((dir) => {
                let dirReader: DirectoryReader = dir.createReader();
                dirReader.readEntries((data: Entry[]) => {
                    if (data && data.length > 0) {
                        for (let i = 0; i < data.length; i++) {
                            // https://github.com/zyra/ionic-image-loader/issues/183
                            let str = Ionic.WebView.convertFileSrc(data[i].toURL());
                            this._images.push(str);
                        }
                        this._size = data.length;
                        this._images.sort((a: string, b: string) => {
                            if (a.length < b.length) {
                                return -1;
                            } else if (a.length > b.length) {
                                return 1;
                            } else {
                                return a.localeCompare(b);
                            }
                        });
                        this.ref.detectChanges();
                    } else {
                        this.showError("no data entry");
                    }
                }, this.showError);
            }).catch((reason: any) => {
                this.showError(reason);
            });
        }

    }

    showError(reason: any) {
        console.log("error", reason);
        this.alertCtrl.create({
            title: "Chyba",
            message: "3D model se nepodařilo načíst",
            buttons: [
                {
                    text: "OK",
                    role: "cancel"
                }
            ]
        }).present();
    }

    startRotation(event) {
        console.log("start of rotate", event);
        this._events = [];
        clearTimeout(this._finishTimeout);
    }

    rotation(event) {
        // console.log(event);
        if (event.additionalEvent == "panright" || event.additionalEvent == "panleft") {

            let diffBetweenTwoPoints = Math.abs(this._events.length > 0 ? event.distance - this._events[this._events.length - 1].distance : 0);

            if (event.additionalEvent == "panleft") {
                for (let i = 0; i < diffBetweenTwoPoints; i += 8) {
                    this.setNewLeftLeftIndex();
                }
            }
            else if (event.additionalEvent == "panright") {
                for (let i = 0; i < diffBetweenTwoPoints; i += 8) {
                    this.setNewRightIndex();
                }

            }

            this.workWithImage();
            this._events.push(event);
        }
    }



    endRotation(event) {
        if (this._events.length > 3) {
            console.log("end of rotate", event);
            let totalDistance = 0;
            for (let i = this._events.length - 4; i < this._events.length - 1; i++) {
                totalDistance += (this._events[i + 1].distance - this._events[i].distance);
            }

            this.finishRotaion(totalDistance / 3);
        }
    }



    finishRotaion(distance) {
        let newDistace = 0;

        if (distance > 10) {
            newDistace = distance * 0.93;
            for (let i = 0; i < distance - newDistace; i++) {
                if (this._events[this._events.length - 1].additionalEvent == "panleft") {
                    this.setNewLeftLeftIndex();
                }
                else if (this._events[this._events.length - 1].additionalEvent == "panright") {
                    this.setNewRightIndex();
                }
                this.workWithImage();
            }
        }
        else {
            newDistace = distance - 1;
            if (this._events[this._events.length - 1].additionalEvent == "panleft") {
                this.setNewLeftLeftIndex();
            }
            else if (this._events[this._events.length - 1].additionalEvent == "panright") {
                this.setNewRightIndex();
            }
            this.workWithImage();
        }


        if (distance > 0) {
            let model = this;
            this._finishTimeout = setTimeout(function () {
                model.finishRotaion(newDistace)
            }, 50);
        }
    }

    workWithImage() {
        $(".imgs").hide();
        $("#img-" + this._selectedIndex).show();
    }

    setNewLeftLeftIndex() {
        this._selectedIndex = (this._selectedIndex + 1 == this._size) ? 0 : this._selectedIndex + 1;
    }

    setNewRightIndex() {
        this._selectedIndex = (this._selectedIndex == 0) ? this._size - 1 : this._selectedIndex - 1;
    }

    zoom(zoom: boolean) {
        if (zoom) {
            this._defaultZoom += 0.2
        }
        else {
            this._defaultZoom -= 0.2;
            this._defaultZoom = this._defaultZoom > 0.1 ? this._defaultZoom : 0.1;
        }

        console.log(this._defaultZoom);

        $('.imgs').css("transform", "scale(" + this._defaultZoom + "," + this._defaultZoom + ")");
    }

}
