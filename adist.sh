#!/bin/sh
APP=platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk
cordova build --release --device android
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore cert/android-dist.keystore -storepass Heslo123 $APP alias_name
#jarsigner -verify -verbose -certs $APP
rm -f release.apk
$ANDROID_HOME/build-tools/28.0.3/zipalign -v 4 $APP release.apk
